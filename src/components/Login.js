import React, { Component } from 'react';
// import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
// import Register from './Register';


class Login extends Component {


    state = {
        credentials: { username: '', password: '' }
    }

    login = event => {
        fetch('http://127.0.0.1:8000/login/', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(this.state.credentials)
        })
            .then(data => data.json())
            .then(
                data => {
                      console.log(data.token);
                    // this.props.userLogin(data.token);
                }
            )
            .catch(error => console.error(error))
    }

    inputChanged = event => {
        const cred = this.state.credentials;
        cred[event.target.name] = event.target.value;
        this.setState({ credentials: cred });
    }



    render() {
        return (
            <div>
                <h1>Login Form</h1>
                
                    <label>
                        UserName:
        <input type="text" name="username"
                            value={this.state.credentials.username}
                            onChange={this.inputChanged} />
                    </label>
                <br />
                <br/>
                    <label>
                        Password:
        <input type="password" name="password"
                            value={this.state.credentials.password}
                            onChange={this.inputChanged}
                        />
                    </label>
                    <br />

                    <button onClick={this.login} className="btn btn-info">Login</button>
    
               
            </div>
        );
    }
}

 export default Login;
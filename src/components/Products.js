import React, {Component} from 'react';
import {CardDeck, Card, Button} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
class Products extends Component {

    state = {
        Products: []
    }
    loadProducts = () => {
        fetch('http://127.0.0.1:8000/Products/', {
            method: 'GET',
            body: JSON.stringify(this.state.credentials)
        })
            .then(data => data.json())
            .then(
                data => {
                    // console.log(data);
                    this.setState({Products: data})
                }
            )
            .catch(error => console.error(error))
    }

    render() {
        return (
            <div className="container row" style={{marginLeft: '400px'}}>

                {this.loadProducts()}

                {this.state.Products.map((items) => ( 
                    <CardDeck style={{width: '22rem'}}>
                        <p key={items.id}></p>
                        <Card style={{margin: '20px'}}>
                            <Card.Img variant="top"
                                    style={{width:'auto' ,height:'200px'}}
                                      src={items.image}/>
                            <Card.Body>
                                <Card.Title>{items.title}</Card.Title>
                                <Card.Text>
                                    Description:{items.Description}
                                </Card.Text>
                                Price:₹{items.price}
                                <div>
                                    <Button variant="primary" size="md">
                                        Buy Now
                                    </Button>{' '}
                                    <Button variant="primary" size="md">
                                        Add to
                                    </Button>
                                </div>
                            </Card.Body>
                            <Card.Footer>
                                <small className="text-muted">Few Items Left</small>
                            </Card.Footer>
                        </Card>


                    </CardDeck>
                ))}
            </div>


        );

    }
}

export default Products;










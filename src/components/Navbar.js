import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Loginpage from './Loginpage';
import Register from './Register';
import {Button,Navbar,Nav,Form,FormControl } from 'react-bootstrap';
import Products from './Products';
import MyOrderhistory from './MyOrderhistory';



const Navbarmain = () => {

    return (

        <Router>
            <div>

    <Navbar bg="info" variant="dark">
    <Navbar.Brand >Vizag Designes</Navbar.Brand>
    <Nav className="mr-auto">
      <Nav.Link href="/">Home</Nav.Link>
      <Nav.Link href="/orders">Order History</Nav.Link>
      <Nav.Link href="/login">Login</Nav.Link>
    </Nav>
    <Form inline>

    </Form>
  </Navbar>

                <Switch>
                    <Route exact path="/"><Products /></Route>
                    <Route path="/login"><Loginpage /></Route>
                    <Route path="/register"><Register /></Route>
                    <Route path="/orders"><MyOrderhistory /></Route>
                    <Route path="/*"><PageNotFound/></Route>


                </Switch>
            </div>

        </Router>
    );
}

function PageNotFound() {
    return(
        <div className="fusion-column col-lg-12 col-md-4 col-sm-4">
       Oops, This Page Could Not Be Found!
        <div className="error-message display-4">
        404
        </div>
       </div>
    )

}

export default Navbarmain;


























import React, { useState, useEffect} from 'react';

const MyOrderhistory = () =>{
const [myorders,setOrders] = useState([]);
useEffect( () => {
    fetch('http://127.0.0.1:8000/orders/',{
        method: 'GET',
        headers : {
            'content-Type': 'application/json',
            'Authorization':'Token 3c74d193893e73140a38fb2ec41c7cc034ba254c'
        }
    })
    .then(data => data.json())
    .then(data => setOrders(data))
    .catch(error => console.log(error))


},[])
return (
    <div>
    {myorders.map(order => {

        return(
        <div className="card">  <p key={order.id}>Order id:{order.id}</p>
        <p>Status:{order.status}</p>
         <p key={order.id}>Mode of Payment:{order.mode_of_Payment}</p>
        </div>)
    }

    )}

    </div>
)
}
export default MyOrderhistory;
